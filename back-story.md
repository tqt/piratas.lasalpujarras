I have always found a strange fascination with the politika (affairs of the cities) https://en.m.wikipedia.org/wiki/Politics

in a former incarnation I was asked on several occasions to "stand for office" & I always refused.
Decades on I still find a perverse intrigue with the process of "government" but as an Anarcho-Primitivist I often struggle to see the relevance of such institutions of an increasingly one world global system of governance ⚠️

Now at almost 46 I find myself a lone on a mountain, living with a cat and a dog, while maintaing land I do not own, in a country where I do not speak the language & is not my place of birth.

I have little money to speak of, no formal employment, no savings, pension schemes or healthcare.
& since Britain's withdrawal from Europe, I also find myself to be an "illegal" ✊

✅ no better time to become a representative of the Pirate Party, me thinks! 🏴‍☠️

[@adinfinitum@activism.openworlds.info](https://activism.openworlds.info/@adinfinitum)